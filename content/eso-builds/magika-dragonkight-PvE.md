# Introduction

# Gear

## Option 1
|Gear	|Set	|Weight	|Trait	|Enchantment|
|:-----|:-------|:------|:------|:---------|
|Chest	|Perfected False God's Devotion|	Light	|Divine	|Magicka|
Belt	Mother's Sorrow	Light	Divine	Magicka
Shoes	Mother's Sorrow	Light	Divine	Magicka
Pants	Perfected False God's Devotion	Light	Divine	Magicka
Hands	Mother's Sorrow	Light	Divine	Magicka
Head	Zaan	Heavy	Divine	Magicka
Shoulder	Zaan	Medium	Divine	Magicka
Necklace	Perfected False God's Devotion	Jewelry	Bloodthirsty	Spell Damage
Ring	Perfected False God's Devotion	Jewelry	Bloodthirsty	Spell Damage
Ring	Perfected False God's Devotion	Jewelry	Bloodthirsty	Spell Damage
Weapon 1	Mother's Sorrow	Fire	Precise	Fire Enchant
Weapon 2	Crushing Wall	Fire	Infused	Weapon Damage Enchant
