---
title: Creation
subtitle: Quote by Stephen Hawking
date: 20XX-0X-1X
bigimg: [{src: "/img/path.jpg", desc: "Path"}]
---

I think computer viruses should count as life.
I think it says something about human nature
that the only form of life we have created
so far is purely destructive. We've created 
life in our own image.
             -Stephen Hawking