---
title: Tear Or Dear
subtitle: hack the box retired challenge reversing
date: 2019-10-25
tags: ["hackthebox", "htb","challenges","retired","reversing","windows"]
---
hackthebox

# tear or dear - hack the box retired challenge reversing

## Requirements/Setup

In this writeup we use
- TriD
- dnSpy

## Inital Recon
It's always a good idea to run the program and get familar before going into it.
We find the TearToDear is 2 forms, a username & password - with a login button.
If we submit some data we get "Wrong Credentials! Try Again..."


## Determine what sort of executable file
Run Trid or another File type detection tool against TearOrDear.exe

We find this is a 32-bit .net application

## Decompile the application
Make sure to run the dnspy 32 bit version

We can drag out .exe file into the dnSpy tool to view the decompiled source code.
looking at the main entry point InitializeComponent().

```.net <initializeComponent code```

mostly looking at how the GUI is built out, the interesting part is the Button.Onpress Function
We could also get to this point by searching for the string "wrong crendtials" and work our way back.
```button1.click()```

So we see here that we need to meet 2 conditions to have the correct flag
username = this.o and this.check1(s)

we can add a break point at the if statment to check the memory during run time.
run with breakpoint, enter some junk data and submit.

now we can look at the this.o variable is "roiw!@a"

To avoid some confusion later we look at string s = this.Multiply().
we find that username gets set to the password_text input, so what we found previous this.o is actually our password.

If we follow the logic for this.check1(s) we run through a few functions until we end up on this.check() that shows us username = this.aa

Again we can find the value of AA in memory is "rowi!a#"

## Flag
We can check our work by running the program without breakpoints entering
user = piph
pass = roiw!@#

Then we can submit the final flag htb{piph:roiw!@#}

## Resources
[1](https://www.youtube.com/watch?v=9BaM4CP3cwg)
[2](https://www.youtube.com/watch?v=6VFSNoSgp9o)
[3 - File command for windows](https://superuser.com/questions/272338/what-is-the-equivalent-to-the-linux-file-command-for-windows)
