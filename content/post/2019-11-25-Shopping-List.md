---
title: My Wishful Bag
subtitle: Wish-list for Bag 
date: 2019-11-15
tags: ["backpack", "hacker","everyday carry", "wishlist"]
---

Backpack Items - Buy List

Lenovo ThinkPad X1 Carbon
https://www.ebay.com/itm/Lenovo-ThinkPad-X1-Carbon-laptop-Core-i7-3-3Ghz-HD-256GB-SSD-LED-Backlit-keys/122013180265


Cheap Antenna
https://www.aliexpress.com/item/32847600796.html?


LR Antenna 
https://www.amazon.com/Wireless-Directional-Antenna-Booster-Connector/dp/B00VK0FFKO


Solar Panel Charger
https://www.amazon.com/Charger-25000mAh-IXNINE-Smartphones-Outdoor/dp/B07T1HJLZD


iFixIt Tool-Kit
https://www.amazon.com/iFixit-Pro-Tech-Toolkit-Electronics/dp/B01GF0KV6G/


Battery Bank
(Omni 20+)
https://www.amazon.com/dp/B072JWN6LC/


DJI Osmo Pocket
https://www.amazon.com/Pocket-Rotating-Polarizer-Extension-Accessories/dp/B07RX9ZJ7J/


External HDD
https://www.amazon.com/Elements-Desktop-Hard-Drive-WDBWLG0080HBK-NESN/dp/B07G3QMPB5


Anker USB Hub
https://www.amazon.com/Anker-Aluminum-Adapter-MacBook-Chromebook/dp/B07DFYQXY7/
&& https://www.amazon.com/Anker-Adapter-Reader-MacBook-Chromebook/dp/B07H4VQ4BZ/


Notepad
https://www.amazon.com/Rocketbook-Everlast-Reusable-Notebook-Executive/dp/B06ZXWVZ3X/


Keyboard
https://kono.store/products/hexgears-x-1-mechanical-keyboard


Mouse
https://www.logitechg.com/en-us/products/gaming-mice/pro-wireless-mouse.html


Cheap DROP USBS
https://www.alibaba.com/product-detail/64mb-usb-stick-promotion-usb-disk_60154061046.html


Main USBS
128GB & 64GB https://www.amazon.com/Corsair-Flash-Survivor-Stealth-128GB/dp/B00YHL1RJG/


AirPods 
Or 
https://en-us.sennheiser.com/truewireless-details

------- total -------
‭1,953.64‬
$2,000
+ 900$ for Mavic Pro 


w/o

RubberDucky & BashBunny


WiFi Pineapple (Rogue Access Point)


LockPick Sets


Wanted not required: 
ProxMark Pro

Mavic Pro
https://www.amazon.com/DJI-Mavic-Pro-Quadcopter-Battery/dp/B01MUAWOXB/


