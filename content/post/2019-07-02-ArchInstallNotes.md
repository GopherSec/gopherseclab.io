---
title: Arch Install
subtitle: dummies try to install arch, because we use arch btw
date: 2019-02-07
tags: ["arch", "linux"]
---

__Arch Linux Install Notes__

# 0x1 - First Thing

__note__ UEFI guide

`
root@archiso $ systemctl start sshd     // Remote install
`
## parition drive

_note_ GPT for UEFI/win10 dualboot, MBR/DOS for 32bit + legacy bios

Simple parition scheme boot part & all root
```
(parted) mklabel GPT
(parted) mkpart primary 1 512
(parted) name 1 grub
(parted) set 1 boot on
(parted) set 1 bios_grub on
(parted) mkpart rootfs 512 100%
```

## Format drive
```
root@archiso ~ # mkfs.ext4 /devsda1
root@archiso ~ # mkfs.xfs /dev/sda2
```
## Mount Filesystem
Mount the file systems
```
root@archiso ~ # mount /dev/sda2 /mnt
root@archiso ~ # mkdir /mnt/boot
root@archiso ~ # mount /dev/sda1 /mnt/boot
root@archiso ~ # pacstrap /mnt base base-devel
root@archiso ~ # genfstab -U /mnt >> /mnt/etc/fstab 
root@archiso ~ # arch-chroot /mnt
```

## Chroot - System stup
```
[root@archiso /]# ln -sf /usr/share/zoneinfo/Reigon/City /etc/localetime 
[root@archiso /]# hwclock --systohc
[root@archiso /]# echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
[root@archiso /]# locale-gen
[root@archiso /]# echo "LANG=en_US.UTF-8" >> /etc/locale.conf
[root@archiso /]# echo "myHostname" >> /etc/hostname
[root@archiso /]# passwd
```

## Bootloader - GRUB
```
[root@archiso /]# pacman -S grub
[root@archiso /]# grub-install
[root@archiso /]# grub-mkconfig -o /boot/grub/grub.cfg
```