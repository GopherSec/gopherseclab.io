#Academia
_please sort by date authored newest to oldest_
_might want to flag topic_

#2016
[The Art of War:Offensive Techniques in Binary Analysis](https://sites.cs.ucsb.edu/~vigna/publications/2016_SP_angrSoK.pdf)
[Driller: Augmenting Fuzzing Through Selective Symbolic Execution ](https://sites.cs.ucsb.edu/~chris/research/doc/ndss16_driller.pdf) 

#2015
[Firmalice - Automatic Detection of Authentication Bypass Vulnerabilities in Binary Firmware](https://sites.cs.ucsb.edu/~chris/research/doc/ndss15_firmalice.pdf)
