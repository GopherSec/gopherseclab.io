---
title: Big list of resources
subtitle: big af list
comments: false
---

# Capture the Flag - War Games
- [websec](https://websec.fr/)
- [CTF4U](https://ctf.katsudon.org/ctf4u/)
- [crack mes](https://crackmes.one/)
- [over the wire](http://overthewire.org/wargames/)
- [OWASP - Broken Web Applications Project](https://www.owasp.org/index.php/OWASP_Broken_Web_Applications_Project)
- [ROP Emporium](https://ropemporium.com/)
- [Vulnhub](https://www.vulnhub.com/)
- [hackthebox](https://www.hackthebox.eu/login)
- [smash the stack](http://smashthestack.org/faq.html)


# Confrences
- [InfoconDB](https://infocondb.org/)
- [DefCon](https://defcon.org/)
- [BlackHat](https://www.blackhat.com/)
- [Hackers on planet earth](http://hope.net/)
- [Bsides](http://www.securitybsides.com/w/page/12194156/FrontPage)
- [Chaos Computing Club](https://media.ccc.de/)
- [hackinthebox](https://conference.hitb.org/)
- [Insomni'hack](https://insomnihack.ch/)
- [offensivecon](https://www.offensivecon.org/)
- [usenix](https://www.usenix.org/)
- [THOTcon](https://www.thotcon.org/)
- [GoTo;](https://www.youtube.com/user/GotoConferences)
- [first](https://www.first.org/)
- [NDC](https://ndcconferences.com/)
- 
# Free Courses?
- [metasploit unleashed](https://www.offensive-security.com/metasploit-unleashed/)
- [Linux Journey](https://github.com/cindyq/linuxjourney)
- [Hacksplaining](https://www.hacksplaining.com/)
- [open security training](http://opensecuritytraining.info/Training.html)

# Paid Course
- [Cybrary Premium](https://www.cybrary.it/become-penetration-tester/)
- [Pentester Academy](https://www.pentesteracademy.com/)
- [Professor Messer](https://www.professormesser.com/security-plus/sy0-501/sy0-501-training-course/)
- [TestOut - Ethical Hacking Training](http://www.testout.com/courses/ethical-hacker-pro)
- [Kaplan IT Training - Cyber Security path](https://www.kaplanittraining.com/track/security)
- [SANS](https://www.sans.org/it-security)
- [PluralSight](https://www.pluralsight.com/search?categories=course&roles=security-professional)
- [Udemy](https://www.udemy.com/courses/it-and-software/network-and-security/)
- [Lynda](https://www.lynda.com/Security-training-tutorials/2069-0.html)
- [CBTNuggets](https://www.cbtnuggets.com/it-training/systems-administration-engineering/it-security)
- [PortSwigger Academy](https://portswigger.net/web-security) // The spiritual succsessor to Web Applications Hacker's Handbook
- [CompTia CertMaster](https://store.comptia.org/training/c/11294?facetValueFilter=tenant~user-type:individual%2ctenant~content-type:labs%2ctenant~content-type:online-courses%2ctenant~content-type:practice-tests&)
- [ITProTV](https://www.itpro.tv/)

# University Coursework
- [Program and Data Representation | cs216 - University of Virginia ](https://www.cs.virginia.edu/~evans/cs216/)
- [Offensive Computer Security | CIS 4930/5930 - FSU?](https://www.cs.fsu.edu/~redwood/OffensiveComputerSecurity/lectures.html)
- [Modern Binary Exploitation | CSCI4986 - Rensselaer Polytechnic Institute](https://github.com/RPISEC/MBE)
- [Exploit Development | CNIT 127 City College San Francisco](https://www.samsclass.info/127/127_F18.shtml)
- [Practical Malware Analysis | CNIT 126 City College San Francisco](https://samsclass.info/126/126_F19.shtml)
- [Intro to Computer Security | cs642 - University of Wisconsin, Madison](http://pages.cs.wisc.edu/~ace/cs642-spring-2016.html)
- [Computer Systems Security | 6.858 - Massacusetts Insttute of Technology](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-858-computer-systems-security-fall-2014/)
- [Cyber Operations, Ph.D. | Dakota State University](https://catalog.dsu.edu/preview_program.php?catoid=28&poid=2055#CourseRotation)
 
# Misc
- [Trail of Bits - CTF Field Guide](http://trailofbits.github.io/ctf/)
- [Buffer Overflow Exploit Development Practice](https://github.com/freddiebarrsmith/Buffer-Overflow-Exploit-Development-Practice)
- [pentest-wiki](https://github.com/nixawk/pentest-wiki)
