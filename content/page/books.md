Support the Publishers & Authos ~

# Publishers
- [No Starch Press](https://nostarch.com/)
- [Oriley](https://learning.oreilly.com/register/)
- [Wiley](https://www.wiley.com/en-us)
- [packt](https://www.packtpub.com)

## Software Development
- [Algorithms](https://github.com/jeffgerickson/algorithms)
- [The Practice of Programming](https://sanet.st/blogs/mgbook/the_practice_of_programming.2830110.html)


## Python
- [Black Hat Python](https://doc.lagout.org/programmation/python/Black%20Hat%20Python_%20Python%20Programming%20for%20Hackers%20and%20Pentesters%20%5BSeitz%202014-12-11%5D.pdf)
- [Gray Hat Python](https://ia800200.us.archive.org/13/items/Gray_Hat_Python/Gray_Hat_Python.pdf)
- [Violent Python](https://7chan.org/pr/src/Violent_Python_A_Cookbook_for_Hackers_Forensic_Ana.pdf)
- [Python: Penetration testing for Developers](https://coderprog.com/python-penetration-testing-developers/)[2](https://github.com/manash-biswal/Python-Books/blob/master/Python%20-%20Penetration%20Testing%20for%20Developers.pdf)
- [Python Web Penetration Testing Cookbook](http://easyupload.net/024baa6dd2a98c08?pt=2TCjNvsJ%2FHfnZ4aHi%2Fkhl62JREJMRhvkbBd0AYRFtUk%3D)
- [Automate the boring stuff](https://docs.google.com/viewerng/viewer?url=https://programmer-books.com/wp-content/uploads/2018/05/Automate+the+Boring+Stuff+with+Python+(2015).pdf )

## GO-Lang
- [Security with GO](https://www.wowebook.org/security-with-go/)
- 
## Hacking
- [The Hacker Playbook (3ED)](https://drive.google.com/file/d/1F3s68f0aE3V9c3JyZ-J3XI2hHqzJ-JmL/view)
- [The Web Application Hacker's Handbook: Finding and Exploiting Security Flaws 2nd Edition ](https://ia801607.us.archive.org/18/items/TheWebApplicationHackersHandbook2ndEdition/The%20web%20application%20hackers%20handbook%20%282nd%20Edition%29.pdf)
- [The Shellcoders Handbook](https://doc.lagout.org/security/The%20Shellcoder%E2%80%99s%20Handbook.pdf)
- [Advanced Penetration Testing](https://www.mediafire.com/file/36h1cqc54dm6m2j/Advanced+Penetration+Testing+-+Hacking+the+World%27s+Most+Secure+Networks.pdf)
- [pentration testing](https://repo.zenk-security.com/Magazine%20E-book/Penetration%20Testing%20-%20A%20hands-on%20introduction%20to%20Hacking.pdf)
- [Tribe of Hackers](https://www.threatcare.com/tribe-of-hackers-free-pdf/)
- [The Art of Software Security Assessment: Identifying and Preventing Software Vulnerabilities](http://ebook3000.com/The-Art-of-Software-Security-Assessment--Identifying-and-Preventing-Software-Vulnerabilities_303186.html)
- [Hacking Point of Sale: Payment Application Secrets, Threats, and Solutions](https://1.droppdf.com/files/IS0md/wiley-hacking-point-of-sale-payment-application-secrets-threats-and-solutions-2014.pdf)

## Cryptography
- [](https://the-eye.eu/public/Books/HumbleBundle/cryptography_engineering_design_principles_and_practical_applications.pdf)

# Systems
- [Building Evolutionary Architectures: Support Constant Change](https://coderprog.com/building-evolutionary-architectures/)

# Unsorted
- Rootkits & Bootkits
- [attacking network protocols](https://github.com/Gajasurve/CurrentRead/blob/master/Attacking-Network-Protocols-A-Hacker-s-Guide-to-Capture-Analysis-and-Exploitation.pdf)
- [PoC||GTFO](https://github.com/angea/pocorgtfo)

# Download Sites
- [wowebook](https://www.wowebook.org)
- [SoftArchive](https://sanet.st/)
- [CoderProg](https://coderprog.com/)
- [ITebooks](http://it-ebooks.info/)
- [lagout](https://doc.lagout.org/)
- [the-eye](https://the-eye.eu/public/Books)
  