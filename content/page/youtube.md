
# Software Design
- [ThoughtWorks](https://www.youtube.com/user/ThoughtWorks/)

# Information Security Channels
- [Security Weekly](https://www.youtube.com/user/SecurityWeeklyTV/)

# Lockpicking Channels
- [Bosnian Bill](https://www.youtube.com/channel/UCp1orOGJwZvjLAvckyxC4Nw)
- [Schuyler Towne](https://www.youtube.com/user/SchuylerTowne/)

# Sysadmin
- [dataknox](https://youtu.be/qH5Y1No37fo)
- 




0patch by ACROS Security 	few videos, very short, specific to 0patch
BlackHat 	features talks from the BlackHat conferences around the world
Christiaan008 	hosts a variety of videos on various security topics, disorganized
	Companies 	
Detectify 	very short videos, aimed at showing how to use Detictify scanner
Hak5 	see Hak5 above
Kaspersky Lab 	lots of Kaspersky promos, some hidden cybersecurity gems
Metasploit 	collection of medium length metasploit demos, ~25minutes each, instructional
ntop 	network monitoring, packet analysis, instructional
nVisium 	Some nVisum promos, a handful of instructional series on Rails vulns and web hacking
OpenNSM 	network analysis, lots of TCPDUMP videos, instructional,
OWASP 	see OWASP above
Rapid7 	brief videos, promotional/instructional, ~ 5 minutes
Securelist 	brief videos, interviews discussing various cyber security topics
Segment Security 	promo videos, non-instructional
SocialEngineerOrg 	podcast-style, instructional, lengthy content ~1 hr each
Sonatype 	lots of random videos, a good cluster of DevOps related content, large range of lengths, disorganized
SophosLabs 	lots of brief, news-style content, "7 Deadly IT Sins" segment is of note
Sourcefire 	lots of brief videos covering topics like botnets, DDoS ~5 minutes each
Station X 	handful of brief videos, disorganized, unscheduled content updates
Synack 	random, news-style videos, disorganized, non-instructional
TippingPoint Zero Day Initiative 	very brief videos ~30 sec, somewhat instructional
Tripwire, Inc. 	some tripwire demos, and random news-style videos, non-instructional
Vincent Yiu 	handful of videos from a single hacker, instructional

